package com.galvanize;

import java.util.*;

//import static jdk.nashorn.tools.ShellFunctions.input;

public class Algorithm {

    public boolean allEqual(String input) {
        boolean result = true;

        if (input.equals("") || input == null) {
            return false;
        }

        input = input.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(0) != input.charAt(i)) {
                return false;
            }
        }
        return result;
    }


    public Map<String, Long> letterCount(String input) {
        // Create the result object to hold and return the result
        Map<String, Long> result = new HashMap<>();

        // If the input string is blank or null then return result
        // which is empty.  The return ends the method and nothing else
        // will be ran from here.
        if(input == "" || input == null) {
            return result;
        }

        // convert the string to lower case - save it to itself
        input = input.toLowerCase();

        // Create an array of chars - easier to work with
        char[] strToChar = input.toCharArray();

        // Run a for loop that goes the length of the input string
        for (int i = 0; i < input.length(); i++) {
            // Look to see if the key is already in the HashMap
            // If it is the add one to the value of the key we are looking for
            // containkey is a built in hashmap method
            // We used an array of char to search for they key but or return needs
            // to be a string so I converted the char into a string with string.valueOf
            // input.char(i) is finding the letter in the string at index i
            if(result.containsKey(String.valueOf(input.charAt(i)))) {
                result.put(String.valueOf(input.charAt(i)), result.get(String.valueOf(input.charAt(i)))+1);
            }else{
                // If the key is not in the Hashmap add the key (Letter at index)
                // and put the initial value to 1
                result.put(String.valueOf(input.charAt(i)),1L);
            }
        }
    return result;
    }



    public String interleave(List<String> inputOne, List<String> inputTwo){
        // Create a string and initialize it as blank
        // If a string comes in blank then it will have nothing to do in the
        // for loop and will return a blank string in result.
        String result = "";

        //  Make a for loop that length is the size of the array
        // Each time it loops "get" the letter in index of i and add it to the blank result string
        // Once it is complete return that result
        for (int i = 0; i < inputOne.size(); i++) {
            result += inputOne.get(i) + inputTwo.get(i);
        }


        return result;
    }
}