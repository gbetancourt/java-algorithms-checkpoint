package com.galvanize;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {

    @Test
    void doesItReturnTrueOrFalseAllTheSame(){

        Algorithm algorithm = new Algorithm();


        boolean test1 = algorithm.allEqual("aAa");      // => returns true
        boolean test2 = algorithm.allEqual("bbBbabbb"); // => returns false
        boolean test3 = algorithm.allEqual("");         // => returns false

        assertTrue(test1);
        assertFalse(test2);
        assertFalse(test3);
    }

    @Test
    void howManyLettersAreTheSame(){
        Algorithm algorithm = new Algorithm();

        Map<String, Long> test1 = algorithm.letterCount("aa");      // => returns { a=2 }
        Map<String, Long> inTest1 = new HashMap<>();
        inTest1.put("a",2L);
        Map<String, Long> test2 = algorithm.letterCount("abBcd");   // => returns { a=1, b=2, c=1, d=1 }
        Map<String, Long> inTest2 = new HashMap<>();
        inTest2.put("a",1L);
        inTest2.put("b", 2L);
        inTest2.put("c", 1L);
        inTest2.put("d", 1L);
        Map<String, Long> inTest3 = new HashMap<>();
        Map<String, Long> test3 = algorithm.letterCount("");        // => returns {}

        assertEquals(inTest1, test1);
        assertEquals(inTest2, test2);
        assertEquals(inTest3, test3);
    }

    @Test
    public void tryToInterweaveTwoStringsTogether(){

        Algorithm algorithm = new Algorithm();



        String test1 = algorithm.interleave(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));  // => returns "adbecf"
        String test2 = algorithm.interleave(Arrays.asList("a", "c", "e"), Arrays.asList("b", "d", "f"));  // => returns "abcdef"
        String test3 = algorithm.interleave(Collections.emptyList(), Collections.emptyList());            // => returns ""

        assertEquals("adbecf", test1);
        assertEquals("abcdef", test2);
        assertEquals("", test3);
    }
}
